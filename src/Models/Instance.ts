export interface Instance {
    id: number;
    name: string;
    categoryId: number;
    startDate: Date;
    endDate: Date;
    order: number;
}