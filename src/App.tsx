import { useState } from 'react';
import search from './search.png';
import { categoriesFromApi, instancesFromApi } from './dummyAPI';
import SelectableList from './Components/SelectableList';
import { Category } from './Models/Category';
import { Instance } from './Models/Instance';
import './App.css';

const App = () => {
  const [categories, setCategories] = useState(categoriesFromApi as Category[]);
  const [selectedCategoryId, setSelectedCategoryId] = useState(1);
  const [instances, setInstances] = useState(instancesFromApi);
  const [filteredInstances, setFilteredInstances] = useState(
    instancesFromApi.filter(
      (instance) => instance.categoryId === selectedCategoryId
    )
  );

  const handleChangeSearchText = (event: { target: { value: string } }) => {
    setCategories(
      categoriesFromApi.filter((category) =>
        category.name.toLowerCase().includes(event.target.value.toLowerCase())
      ) as Category[]
    );
  };

  const handleSelectCategory = (categoryId: number) => {
    setSelectedCategoryId(categoryId);
    setFilteredInstances(
      instances
        .filter((instance) => instance.categoryId === categoryId)
        .sort((a, b) => {
          return a.order - b.order;
        })
    );
  };

  const handleSaveInstance = (
    event: any,
    name: string,
    startDate: string,
    endDate: string
  ) => {
    event?.preventDefault();

    if (
      name.trim().length === 0 ||
      startDate.trim().length === 0 ||
      endDate.trim().length === 0
    ) {
      return alert('You must fill out the name and the 2 date fields too!');
    }

    const newItem = {
      id: Math.random(),
      name: name,
      categoryId: selectedCategoryId,
      startDate: new Date(startDate),
      endDate: new Date(endDate),
      order: filteredInstances.length + 1,
    };

    setInstances((oldInstances) => [...oldInstances, newItem]);
    setFilteredInstances((oldInstances) => [...oldInstances, newItem]);
  };

  const handleDeleteCategory = (event: any, categoryId: number) => {
    event.stopPropagation();

    if (
      !!instances.filter((instance) => instance.categoryId === categoryId)
        .length
    ) {
      if (
        // eslint-disable-next-line no-restricted-globals
        !confirm('Are you sure to delete the category? It has items in it')
      ) {
        return;
      }
    }

    setCategories((oldCategories) =>
      oldCategories.filter((category) => category.id !== categoryId)
    );

    if (categoryId === selectedCategoryId) {
      const newSelectedCategoryId = categories[1]?.id ?? 0;
      setSelectedCategoryId(newSelectedCategoryId);
      setFilteredInstances(
        instances
          .filter((instance) => instance.categoryId === newSelectedCategoryId)
          .sort((a, b) => {
            return a.order - b.order;
          })
      );
    }
  };

  const drop = (copyListItems: any[]) => {
    const copiedInstances = instances;
    for (let index = 0; index < copyListItems.length; index++) {
      const foundIndex = copiedInstances.findIndex(
        (x) => x.id === copyListItems[index].id
      );
      copiedInstances[foundIndex] = { ...copyListItems[index], order: index };
    }

    setInstances(copiedInstances);
    setFilteredInstances(
      instances
        .filter((instance) => instance.categoryId === selectedCategoryId)
        .sort((a, b) => {
          return a.order - b.order;
        })
    );
  };

  return (
    <div className="App">
      <div className="container">
        <form className="form">
          <input
            type="search"
            placeholder="Search"
            className="search-field"
            onBlur={handleChangeSearchText}
          />
          <div className="search-button">
            <img src={search} alt="search" />
          </div>
        </form>
      </div>
      <SelectableList
        title="Categorie"
        itemsToList={categories}
        selectedItemId={selectedCategoryId}
        selectable
        selectItem={handleSelectCategory}
        deletable
        deleteItem={handleDeleteCategory}
      />
      <SelectableList
        title="Instantie"
        itemsToList={filteredInstances}
        selectedItemId={selectedCategoryId}
        showAddButton
        saveItem={handleSaveInstance}
        draggable
        handleDrop={(data) => drop(data as Instance[])}
      />
    </div>
  );
};

export default App;
