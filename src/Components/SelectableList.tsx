import { useRef, useState } from 'react';
import { Category } from '../Models/Category';
import { Instance } from '../Models/Instance';

interface Props {
  title: string;
  itemsToList: Category[] | Instance[];
  selectedItemId: number;
  selectItem?: (id: number) => void;
  deleteItem?: (event: any, id: number) => void;
  showAddButton?: boolean;
  saveItem?: (
    event: any,
    name: string,
    startDate: string,
    endDate: string
  ) => void;
  deletable?: boolean;
  draggable?: boolean;
  handleDrop?: (listItems: any[]) => void;
  selectable?: boolean;
}

const SelectableList = ({
  title,
  itemsToList,
  selectedItemId,
  selectItem,
  deleteItem,
  showAddButton = false,
  saveItem,
  deletable = false,
  draggable = false,
  handleDrop,
  selectable = false,
}: Props) => {
  const [showCreateNewItemForm, setShowCreateNewItemForm] = useState(false);
  const [name, setName] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');

  const handleSaveItem = (event: any, name: string, startDate: string, endDate: string) => {
    if(saveItem) {
      saveItem(event, name, startDate, endDate);

      setName('');
      setStartDate('');
      setEndDate('');
  
      setShowCreateNewItemForm(false);
    }
  }

  const dragItem = useRef() as any;
  const dragOverItem = useRef() as any;

  const dragStart = (e: any, position: any) => {
    dragItem.current = position;
  };

  const dragEnter = (e: any, position: any) => {
    dragOverItem.current = position;
  };

  const drop = (e: any) => {
    const copyListItems = [...itemsToList];
    const dragItemContent = copyListItems[dragItem.current];
    copyListItems.splice(dragItem.current, 1);
    copyListItems.splice(dragOverItem.current, 0, dragItemContent);
    dragItem.current = null;
    dragOverItem.current = null;

    if (handleDrop) {
      handleDrop(copyListItems);
    }
  };

  return (
    <div>
      <h3>{title}</h3>
      {showAddButton && (
        <button onClick={() => setShowCreateNewItemForm(true)}>
          Nieuw instantie
        </button>
      )}
      {showCreateNewItemForm && saveItem && (
        <form className="form add-new-item">
          <div>
            <input
              type="text"
              placeholder="Name"
              value={name}
              onChange={(event) => setName(event.target.value)}
            />
            <input
              type="date"
              placeholder="Start date"
              value={startDate}
              onChange={(event) => setStartDate(event.target.value)}
            />
            <input
              type="date"
              placeholder="End date"
              value={endDate}
              onChange={(event) => setEndDate(event.target.value)}
            />
          </div>
          <div>
            <button onClick={() => setShowCreateNewItemForm(false)}>
              Cancel
            </button>
            <button
              onClick={(event) => handleSaveItem(event, name, startDate, endDate)}
            >
              Save
            </button>
          </div>
        </form>
      )}

      {itemsToList.map((item, index) => (
        <div
          key={item.id}
          className={`list-item ${selectable && 'selectable'} ${
            selectable && item.id === selectedItemId ? 'selected' : ''
          }`}
          onClick={() => selectItem && selectItem(item.id)}
          draggable={draggable}
          onDragStart={(event) => dragStart(event, index)}
          onDragEnter={(event) => dragEnter(event, index)}
          onDragEnd={drop}
        >
          <p>{item.name}</p>
          {deletable && (
            <button onClick={(event) => deleteItem && deleteItem(event, item.id)}>
              Delete
            </button>
          )}
        </div>
      ))}
    </div>
  );
};

export default SelectableList;
